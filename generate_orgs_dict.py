import os
import json
import pandas as pd

CHUNK_SIZE = 8192

WIKIDATA = 'data/output/new_orgs_properties.jsonld'

def read_wikidata(wikidata_file, chunk_size=CHUNK_SIZE):
    chunk = []
    with open(wikidata_file, mode='rt') as finp:
        for line in finp:
            chunk.append(line)
            if len(chunk) >= chunk_size:
                yield chunk
                chunk = []
        # check for remaining lines
        if len(chunk) > 0:
            yield chunk
    # the end
    return None

def extract(line):
    json_line = None
    try:
        line = line.rstrip(',\n')
        json_line = json.loads(line)
    except json.decoder.JSONDecodeError:
        json_line = None
    if json_line is None:
        return None, None

    wid = json_line['id']
    name = json_line['name']
    # wname = json_line['wikipedia']
    return wid, name

def write_list(values, dst):
    with open(dst, 'w') as fout:
        for val in values:
            print(val, file=fout)

def main():
    total_count = 0
    ids_vs_name = dict()

    for lines in read_wikidata(WIKIDATA):
        for line in lines:
            wid, name = extract(line)
            # wlink = None
            # if wname is not None:
            #     wlink = 'https://en.wikipedia.org/wiki/' + '_'.join(wname.split(" "))
            if wid is not None:
                ids_vs_name[wid[1:]] = name
                # org_ids.add(wid[1:])
                # org_names.add(name)
                total_count = total_count + 1
                 
    with open("data/lists/latest/28_04_org_ids_vs_names.json", "w") as f:
        json.dump(ids_vs_name, f, indent= 4)
    # write_list(list(property_list), "data/lists/org_ids_vs_names.txt")
    print(f'r:{total_count}')

if __name__ == '__main__':
    main()
    