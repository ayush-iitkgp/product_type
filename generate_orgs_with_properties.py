import json
import time

CHUNK_SIZE = 8192
LANG = 'en'
WIKIDATA = "data/output/latest/28_04_orgs_properties.jsonld"
OUTFILE = 'data/output/latest/29_04_names_peter_orgs_required_properties.jsonld'

required_properties = {
    "owned by": "ownedBy", 
    "parent organization": "parent",
    "subsidiary": "subsidiary" ,
    "owner of": "ownerOf", 
    "brand": "brand", 
    "country of origin": "countryOrigin", 
    "manufacturer":"manufacturer"
    }


def read_organizations(organization_file):
    organization_dict = dict()
    with open(organization_file, "r") as f:
        organization_dict = json.load(f)
    return organization_dict

organization_dict = read_organizations('data/lists/latest/28_04_org_ids_vs_names.json')

inv_organization_dict = {v: k for k, v in organization_dict.items()}

def read_wikidata(wikidata_file, chunk_size=CHUNK_SIZE):
    chunk = []
    with open(wikidata_file, mode='rt') as finp:
        for line in finp:
            chunk.append(line)
            if len(chunk) >= chunk_size:
                yield chunk
                chunk = []
        # check for remaining lines
        if len(chunk) > 0:
            yield chunk
    # the end
    return None

def write_entities(entities, outfile):
    with open(outfile, 'a') as fout:
        for e in entities:
            print(e, file=fout)

def get_attributes(json_line, lang=LANG):
    wid = json_line['id']
    name = json_line['name']
    aliases = json_line['aliases']
    link = json_line['link']
    present_properties = []
    properties = json_line['properties']
    if properties is None:
        return None
    for property_ in properties:
        for p in property_:
            # print(p)
            if p in required_properties:
                a = dict()
                try:
                    a[required_properties[p]] = 'Q' + inv_organization_dict[property_[p]]
                    present_properties.append(a)
                except:
                    pass                  
    atts = {
        'id': wid,
        'name': name,
        'aliases': aliases,
        'wikipedia': link,
        'properties': present_properties
    }

    if present_properties == []:
        return None 
    atts = dict()
    atts['id'] = wid
    atts['name'] = name
    atts['aliases'] = aliases
    atts['wikipedia'] = link
    atts['properties'] = present_properties

    return atts

def extract(line):
    json_line = None
    try:
        line = line.rstrip(',\n')
        json_line = json.loads(line)

    except json.decoder.JSONDecodeError:
        json_line = None

    if json_line is None:
        return None
    
    entity = get_attributes(json_line)
    if entity is None:
        return None
    entity = json.dumps(entity)
    return entity

def main():
    lines_w = 0 
    lines_r = 0
    null_count = 0
    total_count = 0
    outfile = OUTFILE
    start_time = time.time()
    for lines in read_wikidata(WIKIDATA):
        entities = []
        for line in lines:
            e = extract(line)
            if e is not None:
                entities.append(e)
                total_count = total_count + 1
                # e_json = json.loads(e)
                # if e_json['wikipedia'] is None:
                #     null_count = null_count + 1

        lines_r += len(lines)
        if len(entities) > 0:
            write_entities(entities, outfile)
            lines_w += len(entities)
        if lines_r % 81960 == 0:
            if len(entities) > 0:
                # continue
                write_entities([entities[0]], "data/output/every_5120_orgs_peter_required_properties.jsonld")
            end_time = time.time()
            print("Time taken to process 81960 lines is %d seconds" %(end_time-start_time))
            start_time = end_time
        print(f'r:{lines_r} w:{lines_w} n:{null_count}')

if __name__ == "__main__":
    main()