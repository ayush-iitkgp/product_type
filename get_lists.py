#! /usr/bin/env python3
# This Python file uses the following encoding: utf-8

__author__ = 'jgeiss'


#############################################################################
# authors: Johanna Geiß, Heidelberg University, Germany                     #
# email: geiss@informatik.uni-heidelberg.de                                 #
# Copyright (c) 2017 Database Research Group,                               #
#               Institute of Computer Science,                              #
#               University of Heidelberg                                    #
#   Licensed under the Apache License, Version 2.0 (the "License");         #
#   you may not use this file except in compliance with the License.        #
#   You may obtain a copy of the License at                                 #
#                                                                           #
#   http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                           #
#   Unless required by applicable law or agreed to in writing, software     #
#   distributed under the License is distributed on an "AS IS" BASIS,       #
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.#
#   See the License for the specific language governing permissions and     #
#   limitations under the License.                                          #
#############################################################################
#      02.03.2017                                     #
# last updated 21.3.2017 by Johanna Geiß              #
#######################################################

from requests import get


def get_classes(root_items, forward_properties=None,
                backward_properties=None):
    """Return ids of WikiData items, which are in the tree spanned by the
    given root items and claims relating them to other items.

    :param root_items: iterable[int] One or multiple item entities that are the
                       root elements of the tree
    :param forward_properties: iterable[int] | None property-claims to follow
                               forward; that is, if root item R has
                               a claim P:I, and P is in the list, the search
                               will branch recursively to item I as well.
    :param backward_properties: iterable[int] | None property-claims to follow
                                in reverse; that is, if (for a root
                                item R) an item I has a claim P:R, and P is in
                                the list, the search will branch recursively to
                                item I as well.
    :return: iterable[int]: List with ids of WikiData items in the tree
    """

    query = '''PREFIX wikibase: <http://wikiba.se/ontology#>
            PREFIX wd: <http://www.wikidata.org/entity/>
            PREFIX wdt: <http://www.wikidata.org/prop/direct/>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>'''
    if forward_properties:
        query += '''SELECT ?WD_id WHERE {
                  ?tree0 (wdt:P%s)* ?WD_id .
                  BIND (wd:%s AS ?tree0)
                  }''' % (','.join(map(str, forward_properties)),
                          ','.join(map(str, root_items)))
    elif backward_properties:
        query += '''SELECT ?WD_id WHERE {
                    ?WD_id (wdt:P%s)* wd:Q%s .
                    }''' % (','.join(map(str, backward_properties)),
                            ','.join(map(str, root_items)))
    # print(query)

    url = 'https://query.wikidata.org/bigdata/namespace/wdq/sparql'
    data = get(url, params={'query': query, 'format': 'json'}).json()

    ids = []
    for item in data['results']['bindings']:
        this_id = item["WD_id"]["value"].split("/")[-1].lstrip("Q")
        try:
            this_id = int(this_id)
            ids.append(this_id)
        except ValueError:
            msg = ("ERROR\tWikidata Processor: "
                   "get_classes\t"
                   f"Could not convert data to an integer: {this_id}")
            print(msg)
    return ids


def write_list(values, dst):
    with open(dst, 'w') as fout:
        for val in values:
            print(val, file=fout)


def main():
    # org
    org_list = get_classes([43229], backward_properties=[279])
    write_list(org_list, 'data/lists/latest/org.classes.txt')

    # # loc
    loc_list = get_classes([2221906], backward_properties=[279])
    food_list = get_classes([2095], backward_properties=[279])
    loc_list = list(set(loc_list) - set(food_list))
    write_list(loc_list, 'data/lists/latest/loc.classes.txt')

    # # per
    per_list = [5]
    write_list(per_list, 'data/lists/latest/per.classes.txt')

    # product
    product_list = get_classes([2424752], backward_properties=[279])
    wikipedia_list = get_classes([13406463], backward_properties=[279])
    wikipedia_page = get_classes([4167410], backward_properties=[279])
    lang_list = get_classes([34770], backward_properties=[279])
    languoid_class_list = get_classes([20829075], backward_properties=[279])
    money_list = get_classes([1368], backward_properties=[279])
    national_symbol_list = get_classes([1128637], backward_properties=[279])
    media_wiki_page_list = get_classes([15474042], backward_properties=[279])
    rule_list = get_classes([3150005], backward_properties=[279])
    legal_tran_list = get_classes([327197], backward_properties=[279])
    emblem_list = get_classes([112110], backward_properties=[279])
    monument_list = get_classes([4989906], backward_properties=[279])
    term_list = get_classes([1969448], backward_properties=[279])
    product_list = list(set(product_list) - set(wikipedia_list) - set(wikipedia_page) - 
                    set(languoid_class_list) -set(lang_list) - set(loc_list) - set(money_list) - 
                    set(national_symbol_list) - set(media_wiki_page_list) - set(org_list) - 
                    set(rule_list) - set(legal_tran_list) - set(emblem_list) - set(monument_list) - 
                    set(term_list))
    print("Writing product lists")
    write_list(
        product_list, 'data/lists/latest/product-wikipedia_list-wikipedia_page-locations-languoid-lang-money-national_symbol-wikimedia-org-rule-legal_tran-emblem-monument-term.classes.txt')


if __name__ == '__main__':
    main()
