import bz2
import json
import time
import pandas as pd

WIKIDATA = 'data/wikidata/latest-all.json.bz2'
OUTFILE = 'data/output/latest/28_04_apple_properties.jsonld'
ORG_LIST = 'data/lists/latest/org.classes.txt'
PER_LIST = 'data/lists/latest/per.classes.txt'
LOC_LIST = 'data/lists/latest/loc.classes.txt'
PRODUCT_LIST = 'data/lists/latest/product-wikipedia_list-wikipedia_page-locations-languoid-lang-money-national_symbol-wikimedia-org-rule-legal_tran-emblem-monument-term.classes.txt'
LANG = 'en'
WIKILINK = 'enwiki'
CHUNK_SIZE = 5120

def read_wikidata(wikidata_file, chunk_size=CHUNK_SIZE):
    chunk = []
    with bz2.open(wikidata_file, mode='rt') as finp:
        for line in finp:
            chunk.append(line)
            if len(chunk) >= chunk_size:
                yield chunk
                chunk = []
        # check for remaining lines
        if len(chunk) > 0:
            yield chunk
    # the end
    return None

def write_entities(entities, outfile):
    with open(outfile, 'a') as fout:
        for e in entities:
            print(e, file=fout)

def read_list(list_file):
    vals = open(list_file).readlines()
    vals = [int(v.strip()) for v in vals]
    return set(vals)

def read_lists(per_list_file, org_list_file, loc_list_file, product_list_file):
    list_dict = dict()
    # list_dict['per'] = read_list(per_list_file)
    # list_dict['loc'] = read_list(loc_list_file)
    list_dict['org'] = read_list(org_list_file)
    # list_dict['product'] = read_list(product_list_file)
    return list_dict

def get_subclass_claims(json_line):
    if 'claims' not in json_line:
        return set()
    if 'P31' not in json_line['claims']:
        return set()

    cs = json_line['claims']['P31']
    e_classes = []
    for c in cs:
        try:
            c = c['mainsnak']['datavalue']['value']['numeric-id']
        except KeyError:
            continue

        e_classes.append(c)

    return set(e_classes)

def get_properties(json_line, list_dict, property_dict , organization_dict):
    properties_list = []
    if 'claims' not in json_line:
        return None
    for property_name in json_line['claims']:
        if property_name in ['P31', 'P279', 'P18', 'P373', 'P910', 'P1678', 'P1709', 'P136', 'P1282', 'P1401']:
            continue
        cs = json_line['claims'][property_name]
        for c in cs:
            if 'datavalue' in list(c['mainsnak'].keys()) and c['mainsnak']['datavalue']['type'] == 'wikibase-entityid':
                try:
                    c = c['mainsnak']['datavalue']['value']['numeric-id']
                except KeyError:
                    continue
                if c in list_dict['org']:
                    pro_vs_org = dict()
                    try:
                        pro_vs_org[property_dict[property_name]] = organization_dict[str(c)]
                        properties_list.append(pro_vs_org)
                    except:
                        pass
                    # p_set.add(property_name)

    if len(properties_list) == 0:
        return None        
    return properties_list

def in_list(e_classes, class_list):
    intersection = class_list & e_classes
    return len(intersection) > 0

def get_attributes(json_line, lang=LANG):
    wid = json_line['id']
    if lang not in json_line['labels']:
        return None
    name = json_line['labels'][lang]['value']
    # aliases
    aliases = []
    if 'aliases' in json_line:
        if lang in json_line['aliases']:
            aliases = json_line['aliases'][lang]
            aliases = [a['value'] for a in aliases]

    wlink = None
    if 'sitelinks' in json_line:
        if WIKILINK in json_line['sitelinks']:
            wlink = json_line['sitelinks'][WIKILINK]['title']

    atts = {
        'id': wid,
        'name': name,
        'aliases': aliases,
        'wikipedia': wlink
    }
    return atts

def extract(line, list_dict, property_dict, organization_dict):
    json_line = None

    try:
        line = line.rstrip(',\n')
        json_line = json.loads(line)
    except json.decoder.JSONDecodeError:
        json_line = None

    if json_line is None:
        return None
    wid = json_line['id']
    if wid == 'Q312':
        print(wid)
    else:
        return None

    e_classes = get_subclass_claims(json_line)
    e_type = None
    if in_list(e_classes, list_dict['org']):
        e_type = 'org'
    # elif in_list(e_classes, list_dict['loc']):
    #     e_type = 'loc'
    # elif in_list(e_classes, list_dict['org']):
    #     e_type = 'org'

    if e_type is None:
        return None

    entity = get_attributes(json_line)
    if entity is None:
        return None
    if (entity['wikipedia'] is None): 
        entity['link'] = None
    else: 
        entity['link'] = 'https://en.wikipedia.org/wiki/' + '_'.join(entity['wikipedia'].split(" "))
    entity['type'] = e_type
    entity['claims'] = list(e_classes)
    entity['properties'] = get_properties(json_line, list_dict, property_dict, organization_dict)
     
    # if entity['properties'] is not None:
    entity = json.dumps(entity)
    return entity
    # return None

def read_properties(property_file):
    property_dict = dict()
    df = pd.read_csv(property_file)
    for i, row in df.iterrows():
        property_dict[row['property'].split('/')[-1]] = row['propertyLabel']
    return property_dict

def read_organizations(organization_file):
    organization_dict = dict()
    with open(organization_file, "r") as f:
        organization_dict = json.load(f)
    return organization_dict

def main():
    wikidata_file = WIKIDATA
    outfile = OUTFILE
    lines_r = 0
    lines_w = 0
    total_count = 0
    null_count = 0

    property_file = "data/properties.csv"
    property_dict = read_properties(property_file)
    organization_file = "data/lists/latest/28_04_org_ids_vs_names.json"
    organization_dict = read_organizations(organization_file)
    list_dict = read_lists(PER_LIST, ORG_LIST, LOC_LIST, PRODUCT_LIST)
    start_time = time.time()
    for lines in read_wikidata(wikidata_file):
        entities = []
        for line in lines:
            e = extract(line, list_dict, property_dict, organization_dict)
            if e is not None:
                entities.append(e)
                total_count = total_count + 1
                e_json = json.loads(e)
                if e_json['wikipedia'] is None:
                    null_count = null_count + 1
        lines_r += len(lines)
        
        if len(entities) > 0:
            write_entities(entities, outfile)
            lines_w += len(entities)
        if lines_r % 81960 == 0:
            if len(entities) > 0:
                # continue
                write_entities([entities[0]], "data/output/latest/28_04_every_org_5120.jsonld")
            end_time = time.time()
            print("Time taken to process 81960 lines is %d seconds" %(end_time-start_time))
            start_time = end_time
        print(f'r:{lines_r} w:{lines_w} n:{null_count}')

    # print(p_set)

if __name__ == '__main__':
    main()
